<?php

namespace App\Http\Controllers;

use App\Vivienda;
use Illuminate\http\Request;

class Casas extends Controller{

	public function index(){
		$v = Vivienda::all();
		return view('Viviendas',['vivienda' =>$v]);
	}
	public function casas(){

    	return view('InsertVivienda');
    }

    public function insertar(Request $imp){

    	$vi = Vivienda::all();

    	Vivienda::create($imp->all());
    	return view('Viviendas', compact('vi'));
    }
}

