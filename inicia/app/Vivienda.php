<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vivienda extends Model{

	protected $table = 'vivienda';
	protected $primaryKay ='id';

    protected $fillable=['c_habit','c_baños','colonia','precio','tamanio','municpio','departamento','categoria', 'negociable','estado'];
}
