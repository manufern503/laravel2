<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViviendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vivienda', function (Blueprint $table) {
             $table->bigIncrements('id');
              $table->string('c_habit');
               $table->string('c_baños');
                $table->string('colonia');
                 $table->integer('precio');
                  $table->integer('tamanio');
                   $table->string('municpio');
                    $table->string('departamento');
            $table->string('categoria');
                        $table->string('negociable');

            $table->string('estado');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viviendas');
    }
}
