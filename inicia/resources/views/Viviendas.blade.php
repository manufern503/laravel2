<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<a href="nueva" class="btn btn-primary btn-sm">Nueva Vivienda</a>

	<table class="table table-olver table-dark">
	<thead>
      <tr>
		<td>c_habit</td>
		<td>c_baños</td>
		<td>colonia</td>
		<td>precio</td>
		<td>tamanio</td>
		<td>municpio</td>
		<td>departamento</td>
		<td>categoria</td>
		<td>negociable</td>
		<td>estado</td>
</tr>
    </thead>
     <tbody>
         @foreach ($vi as $ve)
     	<tr>
     		<td><?= $ve->c_habit; ?></td>
     		<td><?= $ve->c_baños; ?></td>
     		<td><?= $ve->colonia; ?></td>
     		<td><?= $ve->precio; ?></td>
     		<td><?= $ve->tamanio; ?></td>
     		<td><?= $ve->municpio; ?></td>
     		<td><?= $ve->departamento; ?></td>
     		<td><?= $ve->categoria; ?></td>
     		<td><?= $ve->negociable; ?></td>
     		<td><?= $ve->estado; ?></td>
     	</tr>
        @endforeach



	
     </tbody>
</table>
</body>
</html>